# i3lock-jmfn

**i3wm lock screen script.**

![Preview](media/i3lock-jmfn.mp4)

- Put the script (i3lock-jmfn) at the desired place. \
e.g $HOME/.config/i3/scripts/

- Edit the i3wm file, example below:

_bindsym $mod+shift+x exec ~/.config/i3/scripts/i3lock-jmfn_

- Lock icon at lock-icon.png.

